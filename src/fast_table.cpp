#include <Rcpp.h>
#include <IntegerHist.hpp>
#include <useful.hpp>

using namespace Rcpp;

// =============================================================================
// Comme la fonction table mais en plus rapide.
// Nombre d'histoire contenant 0 détection
// Nombre d'histoire contenant 1 détection, etc.
// =============================================================================
// [[Rcpp::export]]
IntegerVector fast_table(const IntegerVector x) {
  
  stop_if(is_sorted(x) == false, "vector not sorted.");
  
  stop_if(x[0] < 0, "Vector values not all positive.");
  
  int prev_xx = x[0];
  
  IntegerHist h;
  
  for(auto xx : x) {
    if(prev_xx != xx) {
      h.commit();
      prev_xx = xx;
    }
    h();
  }
  
  return h.as_IntegerVector();
}

