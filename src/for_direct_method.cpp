#include <Rcpp.h>
#include <progress.hpp>
#include <progress_bar.hpp>
#include <algorithm> // for transform
#include <functional> // for plus

#include <useful.hpp>
#include <IntegerHist.hpp>
#include <log.hpp>

using namespace std;
using namespace Rcpp;


// [[Rcpp::depends(RcppProgress)]]

/*
// =============================================================================
//
// =============================================================================
void direct_hist_of_time_interval_sub (const int * hists,
                                       const int * hists_end,
                                       const double * times,
                                       const NumericVector &bins,
                                       NumericVector &counts,
                                       int verbose = 0)
{
  int i = 1;

  int i_bin = 0;

  while ((hists + i) != hists_end && hists[i] == hists[0]) {

    double dt = times[i] - times[0];

    stop_if(dt < 0., "Signal not ordered");

    while (i_bin < bins.size () && dt > bins[i_bin])
      i_bin ++;

    if (i_bin == bins.size ()) break; // Next times will not be taken into account

    counts[i_bin]++;

    i++;
  }
}




// =============================================================================
// @title direct_hist_of_any_time_interval
// @description direct_hist_of_any_time_interval
// @export
// [[Rcpp::export]]
NumericVector direct_hist_of_any_time_interval(const IntegerVector hists, const NumericVector times, const NumericVector bins, int verbose = 0) {

  stop_if(hists.size() != times.size(), "histories and times lengths are different.");

  stop_if(bins.size() < 2, "Not enought bins.");

  stop_if(is_sorted(bins) == false, "'bins' not sorted.");

  int prev_hist = 0;

  NumericVector counts(bins.size());

  Progress progress(hists.size() / 1000, hists.size() > 1e5);

  for(int i = 0; i < hists.size(); i ++)
  {
    stop_if(prev_hist > hists[i], "Histories not sorted");

    prev_hist = hists[i];

    check_user_interrupt(100);

    if(verbose > 0 && i % 1000 == 0)
      progress.increment();

    direct_hist_of_time_interval_sub(hists.cbegin() + i,
                                     hists.cend(),
                                     times.cbegin() + i,
                                     bins,
                                     counts);
  }

  return counts;
}
*/

enum class DirectHistError
{
  noError,
  timesNotSorted,
  integerOverflow
};

// =============================================================================
//
// =============================================================================
void direct_hist_of_time_interval_mp_sub_sub (const int * hists,
                                              const int * hists_end,
                                              const double * times,
                                              const NumericVector &bins,
                                              vector<long long int> &counts,
                                              int verbose,
                                              DirectHistError *error)
{
  int hist0 = *hists;
  double time0 = *times;
  int i_bin = 0;

  hists++;
  times++;

  while (hists != hists_end && *hists == hist0 && *error == DirectHistError::noError) {

    double dt = *times - time0;

    if (dt < 0)
    {
      *error = DirectHistError::timesNotSorted;
      return;
    }

    while (i_bin < bins.size() && dt > bins[i_bin])
      i_bin ++;

    if (i_bin == bins.size()) break; // Les écarts de temps suivant sont en dehors de la plage

    counts[i_bin]++;

    hists++;
    times++;
  }
}


// =============================================================================
//
// =============================================================================
auto direct_hist_of_time_interval_mp_sub (const int * hists,
                                          const int * hists_end,
                                          const double * times,
                                          const NumericVector &bins,
                                          int verbose,
                                          DirectHistError *error)
{
  vector<long long int> counts(bins.size());

  while(hists != hists_end)
  {
    direct_hist_of_time_interval_mp_sub_sub(hists, hists_end, times, bins, counts, verbose, error);
    hists++;
    times++;
  }

  // Vérification (faible) si on a pas un integer overflow
  if(find_if(begin(counts), end(counts), [] (const long long int &a) { return a < 0;}) != end(counts))
  {
    *error = DirectHistError::integerOverflow;
  }

  return counts;
}




// [[Rcpp::depends(RcppProgress)]]

// =============================================================================
// @title direct_hist_of_any_time_interval_mp
// @description direct_hist_of_any_time_interval_mp
// @export
// [[Rcpp::export]]
NumericVector direct_hist_of_any_time_interval_mp(const IntegerVector hists,
                                                  const NumericVector times,
                                                  const NumericVector bins,
                                                  int verbose = 0)
{
  Log::set_threshold(verbose);

  stop_if(hists.size() != times.size(), "histories and times lengths are different.");

  stop_if(bins.size() < 2, "Not enought bins.");

  stop_if(is_sorted(bins) == false, "'bins' not sorted.");

  stop_if(is_sorted(hists) == false, "'hist' not sorted.");

  RcppThread::ThreadPool pool;

  vector<future<vector<long long int>>> futures;

  int task_size = hists.size() /  max(1u, thread::hardware_concurrency()) / 10;

  stop_if(task_size == 0, "Task size of zero");

  Log::for_level(1, "Tasks of size {}", task_size);

  DirectHistError error = DirectHistError::noError;

  auto it_beg = hists.cbegin();
  auto it_end = hists.cbegin();

  while(it_beg != hists.cend() && error == DirectHistError::noError)
  {
    it_end = min(hists.cend(), it_beg + task_size);

    while(it_end != hists.cend() && *it_end == *(it_end - 1))
      it_end ++;

    auto it_time_beg = times.cbegin() + std::distance(hists.cbegin(), it_beg);

    Log::for_level(1, "Add task of size {} with hist start @ hist {} and time {}.", std::distance(it_beg, it_end), *it_beg, *it_time_beg);

    futures.emplace_back(pool.pushReturn(direct_hist_of_time_interval_mp_sub,
                                         it_beg,
                                         it_end,
                                         it_time_beg,
                                         bins,
                                         verbose,
                                         &error));
    it_beg = it_end;
  }

  Progress progress(futures.size(), hists.size() > 1e4);

  vector<double> counts(bins.size(), 0);

  for(auto & future : futures)
  {
    auto a = future.get();

    std::transform(counts.begin(), counts.end(), a.begin(), counts.begin(), std::plus<double>());

    if(verbose > 0)
      progress.increment();
  }

  pool.join();

  stop_if(error == DirectHistError::timesNotSorted, "Times not sorted.");

  stop_if(error == DirectHistError::integerOverflow, "Integer overflow.");

  return wrap(counts);
}

