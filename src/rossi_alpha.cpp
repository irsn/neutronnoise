#include <Rcpp.h>
#include <RcppThread.h>
#include "useful.hpp"
#include "log.hpp"

using namespace Rcpp;
using namespace std;


// Find the last trigger (signal duration - gate duration)
inline int find_last_trigger (const NumericVector times, double gate_length) {
  int last_trigger = times.size () - 1;
  double last_count_time = times [last_trigger];
  while ((last_count_time - times [last_trigger]) < gate_length)
    last_trigger --;
  return last_trigger;
}

inline void check_user_interrupt()
{
  static int i_check_user_interrupt = 0;

  if(++i_check_user_interrupt % 1000 == 0)
    Rcpp::checkUserInterrupt();
}

// =================================================================================================
//' Rossi-Alpha curve calculation.
//'
//' @description
//' This function creates a histogram of time differences between a first (called Trigger) detection and subsequent ones.
//'
//' @param x A signal. The function assumes that the signal starts at the time 0 seconds.
//' @param bins Numeric vector of upper bound of the histogram. First bin value must be > 0.
//' @param normalize_to_trigger If TRUE divid the histogram by the number of trigger used.
//' @param normalize_to_bin If TRUE divid each value of the histogram by the corresponding bin size.
//' @param normalize_to_duration If TRUE divid each value by the duration of the numeric vector that is max(times) - min(times).
//' @param verbose Verbosity level.
//'
//' @return A List containgin :
//'  - The number of trigger used
//'  - A Data Frame containing the Rossi-Alpha values
//' @export
// [[Rcpp::export]]
List rossi_alpha (const DataFrame x,
                  const NumericVector bins,
                  bool normalize_to_trigger = false,
                  bool normalize_to_bin = true,
                  bool normalize_to_duration = true,
                  int verbose = 0) {

  Log::set_threshold(verbose);

  // Vérification des bins
  stop_if(bins.size() == 0, "No bins value.");
  stop_if(bins [0] <= 0, "Bins must be strictly positive.");
  stop_if(is_sorted(bins.begin(), bins.end()) == false, "Bins must be ordered.");

  const NumericVector times = x["TIME"];

  // Vérification des times
  stop_if(times.size() == 0, "No times value.");
  stop_if(times [0] < 0, "Time values must be positive.");
  stop_if(is_sorted(times.begin(), times.end()) == false, "Times values must be ordered.");

  NumericVector RA(bins.size());

  double gate_length = bins[bins.size() - 1];

  // Find the last trigger (signal duration - gate duration)
  int last_trigger = find_last_trigger(times, gate_length);

  int i_trigger = 0;

  while (i_trigger < last_trigger) {

    check_user_interrupt();

    int ibin = 0;
    int i_time = i_trigger + 1;
    double dt = times [i_time] - times [i_trigger];

    while (dt < gate_length) {

      while (dt > bins [ibin]) ibin ++;

      RA [ibin] ++;

      i_time ++;

      dt = times [i_time] - times [i_trigger];
    }

    i_trigger ++;
  }

  if (normalize_to_duration)
    RA = RA / (times[times.size()-1] - times[0]);

  if (normalize_to_trigger)
    RA = RA / last_trigger;

  if (normalize_to_bin) {
    NumericVector tmp = bins;
    tmp.push_front (0);
    RA = RA / diff (tmp);
  }

 DataFrame ret = DataFrame::create(_["bins"] = bins,
                                   _["values"] = RA);

 ret.attr("class") = CharacterVector::create ("rossi_alpha", "data.frame");

  return ret;
}





/*

auto find_last_trigger_task (const NumericVector times, double gate_length) {

  auto last_trigger = times.cend() - 1;

  double criterion = *last_trigger - gate_length;

  while(last_trigger != times.begin() && *last_trigger >= criterion)
    last_trigger--;

  return last_trigger;
}


auto rossi_alpha_sub_task (const double * it_beg, const double * it_end, NumericVector bins, double gate_length, int thread_id, int verbose)
{
  vector<double>  RA(bins.size(), 0);

  for (auto it_trigger = it_beg; it_trigger != it_end; it_trigger ++) {

    if(reinterpret_cast<long int> (it_trigger) % 1000)
      RcppThread::checkUserInterrupt();

    int ibin = 0;

    auto it_time = it_trigger + 1;

    double dt = *it_time - *it_trigger;

    while (dt < gate_length) {

      while (dt > bins [ibin])
        ibin ++;

      RA[ibin] ++;

      it_time ++;

      dt = *it_time - *it_trigger;
    }
  }

  return RA;
}



// =================================================================================================
//' Rossi-Alpha curve calculation.
//'
//' @description
//' This function creates a histogram of time differences between a first (called Trigger) detection and subsequent ones.
//'
//' @param x A signal. The function assumes that the signal starts at the time 0 seconds.
//' @param bins Numeric vector of upper bound of the histogram. First bin value must be > 0.
//' @param normalize_to_trigger If TRUE divid the histogram by the number of trigger used.
//' @param normalize_to_bin If TRUE divid each value of the histogram by the corresponding bin size.
//' @param normalize_to_duration If TRUE divid each value by the duration of the numeric vector that is max(times) - min(times).
//'
//' @return A data.frame.
//'
//' @export
// [[Rcpp::export]]
List rossi_alpha(const DataFrame x,
                  const NumericVector bins,
                  bool normalize_to_trigger = false,
                  bool normalize_to_bin = true,
                  bool normalize_to_duration = true,
                  int verbose = 0)
{

  Log::set_threshold(verbose);

  // Vérification des bins
  stop_if(bins.size() == 0, "No bins value.");
  stop_if(bins[0] <= 0 || is_sorted(bins.begin(), bins.end()) == false, "Argument bins must be strictly positive and increasing.");

  const NumericVector times = x["TIME"];

  // Vérification des times
  stop_if(times.size() == 0, "No times value.");
  stop_if(times[0] < 0 || is_sorted(times.begin(), times.end()) == false, "Argument times must be positive and increasing.");

  double gate_length = bins[bins.size() - 1];

  auto last_trigger = find_last_trigger_task(times, gate_length);

  int nb_task = max(1u, thread::hardware_concurrency() - 1);

  Log::for_level(1, "Use {} thread(s).", nb_task);

  int task_size = (last_trigger - times.cbegin()) / nb_task;

  stop_if(task_size == 0, "Bins overflow, consider reducing largest bin size.");

  RcppThread::ThreadPool pool;

  vector<future<vector<double>>> futures(nb_task);

  for(int i_task = 0; i_task < nb_task; i_task++) {

    auto it_beg = times.begin() + i_task * task_size;

    auto it_end = ( i_task == (nb_task - 1) ? last_trigger : times.begin() + (i_task + 1) * task_size );

    Log::for_level(1, "Add task {} with data included in [{}, {}].", i_task, *it_beg, *it_end);

    futures[i_task] = pool.pushReturn(rossi_alpha_sub_task, it_beg, it_end, bins, gate_length, i_task, verbose);
  }

  NumericVector RA(bins.size(), 0);

  for(auto & future : futures) {
    NumericVector a = wrap(future.get());
    RA = RA + a;
  }

  pool.join();

  if(normalize_to_duration)
    RA = RA / (*last_trigger - times[0]);

  if(normalize_to_trigger)
    RA = RA / distance(last_trigger, times.cbegin());

  if(normalize_to_bin) {
    NumericVector tmp = bins;
    tmp.push_front(0);
    RA = RA / diff(tmp);
  }


  DataFrame ret = DataFrame::create(_["bins"] = bins,
                                    _["values"] = RA);

  ret.attr("class") = CharacterVector::create ("rossi_alpha", "data.frame");

  return ret;
}

*/
