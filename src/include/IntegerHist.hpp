
#ifndef INTEGER_HIST_HPP
#define INTEGER_HIST_HPP

#include <vector>
#include <Rcpp.h>

class IntegerHist {
  
  std::vector<unsigned long long int> data;
  unsigned long long int i = 0;
  unsigned int nb_commit = 0;
  
public:
  
  void operator () () {
    i ++;
  }
  
  void commit() {
    if(data.size() <= i)
      data.resize(i + 1, 0);
    data[i]++;
    i = 0;
    nb_commit ++;
  }
  
  const auto & get_data() const {
    return data;
  }
  
  const unsigned int get_nb_commit() const {
    return nb_commit;
  }
  
  Rcpp::IntegerVector as_IntegerVector() const {
    Rcpp::IntegerVector res;
    if(data.size() > 0)
      res.assign(data.begin(), data.end());
    return res;
  }
};


#endif
