//[[Rcpp::plugins(cpp17)]]

#include <Rcpp.h>
#include <fstream>
#include <bitset>
#include <string>
#include <tuple>
#include <regex>

#include "useful.hpp"
#include "log.hpp"

using namespace Rcpp;
using namespace std;

struct LMXHeader
{
  double binary_data_clock_tick_length = -1;
  double firmware_channel_deadtime = -1;
  int fifo_lost_counts = -1;
  int start_of_binary = -1;
  int duration_real_time = -1;
};

// ============================================================================
// La partie header d'un fichier LMX et retourne des informations nécessaires pour la suite
// ============================================================================
LMXHeader lmx_read_header(string file_name)
{
  ifstream fic(file_name.c_str(), ios::in);

  LMXHeader header;

  string line;

  while(line.find("BinaryDataFollows") == string::npos)
  {
    getline(fic, line, '\n');  // each line terminates with \r\n

    stop_if(!fic, "Error reading file '{}'", file_name);

    line.pop_back();

    Log::for_level(3, "'{}'", line);

    header.start_of_binary += line.length() + 2;

    smatch sm;
    if(regex_match(line, sm, regex(R"(BinaryDataClockTickLength\s+:\s+(\d+)\s\[ns\])")) == 1)
    {
      header.binary_data_clock_tick_length = stoi(sm[1]) * 1e-9;  // s

    } else if(regex_match(line, sm, regex(R"(FifoLostCounts\s+:\s+(-?\d+))")) == 1)
    {
      header.fifo_lost_counts = stoi(sm[1]);

    } else if(regex_match(line, sm, regex(R"(FirmwareChannelDeadtime\s+:\s+(\d+)\s\[ns\])")) == 1)
    {
      header.firmware_channel_deadtime = stoi(sm[1]) * 1e-9;  // s

    } else if(regex_match(line, sm, regex (R"(DurationRealTime\s+:\s+(\d+)\s\[s\])")) == 1)
    {
      header.duration_real_time = stoi(sm[1]);
    }
  }

  header.start_of_binary ++;

  return header;
}

// ============================================================================
// Estime le nombre de détection qu'il y a dans le fichier lmx en se basant sut la taille de celui-ci
// ============================================================================
int lmx_guess_nb_detections(ifstream & fic, LMXHeader header)
{
  streampos pos_save = fic.tellg();

  fic.seekg(header.start_of_binary, ios_base::beg);

  streampos binary_begin = fic.tellg();

  fic.seekg(0, ios_base::end);

  streampos binary_end = fic.tellg();

  fic.seekg(pos_save, ios_base::beg);

  return static_cast<unsigned int>((binary_end - binary_begin) / 8 * 1.1);  // one entry = 2 * 32 bits, add 10 %
}

// ============================================================================
// Lit la partie binaire d'un lmx, c'est cette partie qui contient les enregistrements des détections.
// ============================================================================
auto lmx_read_binary(ifstream & fic, LMXHeader header, int nb_detections_guess)
{
  fic.seekg(header.start_of_binary, ios_base::beg);

  vector<unsigned int> channels;

  vector<double> times;

  channels.reserve(nb_detections_guess);

  times.reserve(nb_detections_guess);

  unsigned  int nb_total_detections = 0;

  bool end_of_file = false;

  // Flag values :
  // Flag = 0x00000001 (Clock rollover occurred)
  // Flag = 0x00000002 (Gate input started)
  // Flag = 0x00000003 (Gate input ended)
  // Flag = 0xFFFFFFFF (End of binary data)
  bool flag = false;

  double previous_time = 0;   // Pour vérifier si les instants de détection sont croissants

  double time_offset = 0;  // offset lorsqu'il y a un clock rollover

  while(!end_of_file)
  {
    check_user_interrupt(1000);

    // Get time and channel
    unsigned int raw_channel, raw_time;
    fic.read((char*)&raw_channel, 4);   // 32 bits
    fic.read((char*)&raw_time, 4);    // 32 bits

    stop_if(!fic, "Error reading file");

    Log::for_level(3, "{} {}", raw_time, raw_channel);

    // Previous special event
    if(flag == true)
    {
      flag = false;

      switch(raw_channel)
      {
      case 0x00000001 :
        time_offset += 4294967296 * header.binary_data_clock_tick_length; // 2^32 = 4294967296
        Log::for_level(2, "Clock rollover, time_offset now equal to {} s", time_offset);
        break;

      case 0xFFFFFFFF :
        end_of_file = true;
        break;

      default :
        stop("Unknow flag ({})", raw_channel);
      }

      continue;
    }

    // This is not a real event and next event will be special
    if(raw_channel == 0)
    {
      flag = true;
      continue;
    }

    // Got a real event

    double time = raw_time * header.binary_data_clock_tick_length + time_offset;

    stop_if(time - previous_time < 0, "Time values not increasing ({} < {})", time, previous_time);

    bitset<32> bit(raw_channel);

    unsigned short int nb_detections = 0;

    for(unsigned char num_channel = 0; num_channel < 32; num_channel++)
    {
      if(bit[num_channel] == 0)
        continue;
      channels.push_back(num_channel + 1);

      times.push_back(time) ;

      nb_total_detections ++;

      nb_detections ++;
    }

    stop_if(nb_detections == 0, "Get an event but no detection");

    previous_time = time;
  }

  return make_tuple(times, channels, nb_total_detections);
}



auto read_LMX_raw(std::string file_name, int verbose = 0)
{
  Log::set_threshold(verbose);

  Log::for_level(1, "Read {}", file_name);

  // Text part (description)
  auto header = lmx_read_header(file_name);

  // The followings data are nedded
  stop_if(header.binary_data_clock_tick_length == -1, "BinaryDataClockTickLength not found");

  stop_if(header.fifo_lost_counts == -1, "FifoLostCounts not found");

  // Binary part (data)

  ifstream fic(file_name.c_str(), ios::in | ios::binary);

  Log::for_level(2, "Binary part starts @ {}", header.start_of_binary);

  // guess the number of detections

  int nb_detections_guess = lmx_guess_nb_detections(fic, header);

  Log::for_level(2, "Guess {} detections", nb_detections_guess);

  stop_if(nb_detections_guess < 1, "No dtections found");

  auto [times, channels, nb_detections] = lmx_read_binary(fic, header, nb_detections_guess);

  return make_tuple(times, channels, nb_detections, header.duration_real_time);
}




// ============================================================================
//' @title Lecture d'un fichier LMX
//' @description Les fichier LMX contiennent les données lus par les détecteurs de type MC15 / NoMad.
//' Les instants de détection doivent être croissants, dans le cas contaire une erreur est déclenchée.
//' @param file Chemin d'accès au fichier LMX à lire.
//' @export
// [[Rcpp::export]]
DataFrame read_LMX(std::string file, int verbose = 0)
{
  auto [times, channels, nb_detections, duration] = read_LMX_raw(file, verbose);

  auto ret = DataFrame::create(_("TIME") = times, _("VOLU") = channels);

  ret.attr("file") = file;
  ret.attr("nb_detections") = nb_detections;
  ret.attr("duration") = duration;

  return ret;
}


// ============================================================================
//' @title Création d'un signal à partir d'un lot de fichiers LMX.
//' @description Voir \code{\link{read_LMX}} pour plus d'information sur les fichiers LMX.
//' @param files Vecteur de chemin d'accès des fichiers LMX. Le signal est crée avec l'ordre des fichiers donnés.
//' @return Un dataframe avec des attributs.
//' @export
// [[Rcpp::export]]
DataFrame signal_from_LMX(CharacterVector files, int verbose = 0)
{
  Log::set_threshold(verbose);

  vector<vector<double>> frag_times;
  vector<vector<unsigned int>> frag_channels;
  vector<int> frag_nb_detections;
  vector<double> frag_durations;
  int nb_total_detections = 0;

  double offset = 0;

  for(auto file : files)
  {
    auto [times, channels, nb_detections, duration] = read_LMX_raw(as<string>(file), verbose);

    stop_if(times.size() != channels.size(), "Inconsistent channels and times vector.");

    stop_if(times.size() != nb_detections, "Inconsistent times and nb_detections");

    for(auto & t : times)
      t += offset;

    frag_times.emplace_back(times);
    frag_channels.emplace_back(channels);
    frag_nb_detections.push_back(nb_detections);
    frag_durations.push_back(duration);

    nb_total_detections += nb_detections;

    offset += duration;
  }

  Log::for_level(2, "Fin de la lecture des LMX, fusion des données. Nombre total de détections {}.", nb_total_detections);

  vector<double> times;
  times.reserve(nb_total_detections);

  vector<unsigned int> channels;
  channels.reserve(nb_total_detections);

  for(unsigned int j = 0; j < frag_times.size(); j++)
  {
    for(unsigned int i = 0; i < frag_times[j].size(); i++)
    {
      times.push_back(frag_times[j][i]);
      channels.push_back(frag_channels[j][i]);
    }
    frag_times[j].clear();
    frag_channels[j].clear();
  }

  auto ret = DataFrame::create(_("TIME") = times, _("VOLU") = channels);

  ret.attr("files") = files;
  ret.attr("nb_detections") = frag_nb_detections;
  ret.attr("durations") = frag_durations;

  return ret;
}



/*** R
*/
