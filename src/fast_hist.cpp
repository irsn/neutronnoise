#include <Rcpp.h>
#include <vector>

#include "useful.hpp"
#include "log.hpp"

using namespace Rcpp;
using namespace std;


auto calculate_breaks(NumericVector & x, int nb_breaks)
{
  stop_if(nb_breaks < 1, "Number of breaks must be > 0");

  double min = x[0];
  double max = x[x.size() - 1];

  double bin_size = (max - min)/nb_breaks;
  vector<double> breaks(nb_breaks + 1);
  for(int i = 0; i<nb_breaks; i++)
    breaks[i] = min + bin_size * i;
  breaks[nb_breaks] = max;
  return breaks;
}

// ============================================================================
//' @title Fast histogram
//' @description Fonction de calcul d'histogramme rapide pour Numeric vector trié par ordre croissant.
//' @param x Vecteur Numeric sur lequel l'histogramme est calculé.
//' @param breaks Si de dimension 1 alors spécifie le nombre de bins, sinon specifie explicitement les breaks.
//' @param range_respect Si TRUE \code{breaks} doit contenir entièrment les valeurs de \code{x}.
//' @return Une liste
//' @export
// [[Rcpp::export]]
List fast_hist(NumericVector x, NumericVector breaks, bool range_respect = true, int verbose = 0)
{
  Log::set_threshold(verbose);

  stop_if(x.size() < 2, "x must be of size > 1");

  // Vérifie que x est croissant
  stop_if(is_sorted(x) == false, "x must be increasing");

  // C'est le nombre de bin qui est fourni => calculs les breaks
  if(breaks.size() == 1)
    breaks = calculate_breaks(x, static_cast<int>(breaks[0]));

  // Il faut au moins deux break
  stop_if(breaks.size() <= 2, "breaks length must be >= 2");

  // Vérification que les breaks sont croissants
  stop_if(is_sorted(x) == false, "breaks must be strictly increasing");

  // Calcul de mids (valeurs milieu des breaks)
  NumericVector mids (breaks.size() - 1);
  for (int i = 0, j = breaks.size() - 1; i < j; i++)
    mids[i] = (breaks[i+1] - breaks[i]) / 2 + breaks[i];

  // Si demandé on vérifie que toutes les valeurs de x seront comptées
  stop_if(range_respect && (x[0] < breaks[0] || x[x.size() - 1] > breaks[breaks.size() - 1]), "Vector has value(s) out of range");

  NumericVector counts(mids.size()); // Valeurs counts
  long long int i = 0;  // position a partir de laquelle on prend en compte les données
  long long int i_max = x.size();

  // Zappe les premières valeurs inférieures au premier break
  while(i < i_max && x[i] < breaks[0])
    i++;

  Log::for_level(2, "Start at position x[{}] = {}.", i + 1, x[i]);

  long long int j = 1;  // Position courante du break
  long long int j_max = breaks.size();

  while(i < i_max)
  {
    // Recherche la borne sup des breaks
    while(j < j_max && x[i] > breaks[j])
      j ++;

    stop_if(range_respect && j == j_max, "Internal error : End of break reached while there is still data left."); // On ne devrait jamais arriver ici

    if(j == j_max)
      break;

    check_user_interrupt(1000);

    i ++;

    counts[j - 1] ++;
  }

  // Vérirfication pour s'assurer qu'on a bien compter ce qu'il fallait compter
  //stop_if(sum(counts) != i, "Internal error, total number of counts ({}) inconsitent with last index ({}).", sum(counts), i);

  int not_counted = i_max - i;

  return List::create(
    _["breaks"] = breaks,
    _["mids"] = mids,
    _["counts"] = counts,
    _["not_counted"] = not_counted);
}

// ============================================================================
//' @title Fast histogram DF
//' @export
// [[Rcpp::export]]
DataFrame fast_hist_df(NumericVector x, NumericVector breaks, bool range_respect = true, int verbose = 0)
{
  auto d = fast_hist(x, breaks, range_respect, verbose);
  return DataFrame::create(
    _["mids"] = d["mids"],
    _["counts"] = d["counts"]
  );
}

/*** R

*/
