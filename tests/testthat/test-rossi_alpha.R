
context("Rossi Alpha")

library(dplyr)

test_that("Normal", {
  
  set.seed(1)
  
  p_ref <- readRDS(file = "test-rossi_alpha_data/rossi_alpha_00.rds")
  
  s <- read.table("data/ISSA_CASE5.moret.detec/ntrac.detec_ncap.gz", header = TRUE) %>%   # tests/testthat/
    dplyr::mutate(TIME = TIME /1e6) %>% as_signal(x_duration = 1000, is_t0 = TRUE)
  
  p <- rossi_alpha (s, bins = lseq(from = 1e-5, to = 5e-3, length = 50))

   # saveRDS(p, file = "tests/testthat/test-rossi_alpha_data/rossi_alpha_00.rds")

  expect_equal(p, p_ref)
})


